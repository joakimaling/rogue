#pragma once

#include "../component.hpp"
#include <cstdint>

namespace rogue {
	class Velocity: public Component {
		public:
			std::uint8_t x, y;
	};
}
