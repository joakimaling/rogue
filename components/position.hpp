#pragma once

#include "../component.hpp"
#include <cstdint>

namespace rogue {
	class Position: public Component {
		public:
			std::uint16_t x, y;

			Position(std::uint16_t x, std::uint16_t y): x(x), y(y) {}
	};
}
