#pragma once

#include "../component.hpp"
#include <cstdint>

namespace rogue {
	class Appearance: public Component {
		public:
			std::uint16_t colour;
			char glyph;

			Appearance(const char glyph, const std::uint16_t colour):
				colour(colour), glyph(glyph) {}
	};
}
