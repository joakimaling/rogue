#include "manager.hpp"

namespace rogue {
	void Manager::addEntity(Entity* entity) {
		std::unique_ptr<Entity> pointer{entity};
		entities.emplace_back(std::move(pointer));
	}
}
