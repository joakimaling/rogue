#include "console.hpp"

namespace rogue {
	void Console::clear() {
		std::cout << "\033[2J";
	}

	void Console::cursor(const bool state) {
		std::cout << "\033[?25" << (state ? 'h' : 'l');
	}

	void Console::put(const std::uint8_t x, const std::uint8_t y, const char glyph, const std::uint16_t colour) {
		std::cout
			<< "\033[" << static_cast<short>(y) << ";" << static_cast<short>(x) << "f"
			<< "\033[48;5;" << (colour >> 8) << "m" << "\033[38;5;" << (colour & 0x0f) << "m"
			<< glyph << "\033[0m";
	}

	void Console::reset(){
		std::cout << "\033[0;0f";
	}
}
