#include "random.hpp"

namespace rogue {
	std::uint8_t Random::operator()(const std::uint8_t from, const std::uint8_t to) {
		std::uniform_int_distribution<std::uint8_t> distribution(from, to);
		return distribution(engine);
	}
}
