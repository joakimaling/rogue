#pragma once

#include <cstdint>
#include <iostream>

namespace rogue {
	class Console {
		private:
			Console() {}
		public:
			static void clear();
			static void cursor(const bool);
			static void put(const std::uint8_t, const std::uint8_t, const char, const std::uint16_t);
			static void reset();
	};
}
