#pragma once

#include <cstdint>
#include <random>

namespace rogue {
	class Random {
	private:
		std::mt19937 engine;
	public:
		Random(): engine(std::random_device()()) {}
		Random(const Random&) = delete;
		Random(const Random&&) = delete;
		Random& operator = (const Random&) = delete;
		std::uint8_t operator()(const std::uint8_t, const std::uint8_t);
	};
}
