CXXFLAGS = -MMD -MP -Wall -Wextra -pedantic -std=c++11
#LDFLAGS =
sources = $(shell find . -path ./systems -prune -false -o -name '*.cpp')
objects = $(sources:.cpp=.o)
depends = $(objects:.o=.d)
target = rogue

# Declare non-file targets
.PHONY: clean debug release run tags

# Link objects into target
$(target): $(objects)
	$(CXX) $(LDFLAGS) -o $@ $^

# Include dependencies
-include $(depends)

# Compile source code & create dependencies
%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

# Remove dependencies, objects & target
clean:
	$(RM) $(depends) $(objects) $(target)

# Compile debuggable code & run debugger
debug: CXXFLAGS += -D DEBUG -Og -g
debug: clean $(target)
	gdb $(target)

# Compile target optimised for release
release: CXXFLAGS += -O3
release: clean $(target)

# Run target
run: $(target)
	./$(target)

# Create tags
tags:
	ctags -R .
