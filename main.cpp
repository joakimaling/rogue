#include "components/appearance.hpp"
#include "components/position.hpp"
#include "entity.hpp"
#include "manager.hpp"
#include "utilities/console.hpp"

using namespace rogue;

int main() {
	Entity* entity = new Entity();

	entity->addComponent<Appearance>('@', 0xd5e2);
	entity->addComponent<Position>(10, 10);

	Manager* manager = new Manager();
	manager->addEntity(entity);

	Console::clear();

	Console::put(
		entity->getComponent<Position>().x,
		entity->getComponent<Position>().y,
		entity->getComponent<Appearance>().glyph,
		entity->getComponent<Appearance>().colour
	);

	return 0;
}
