{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... March 16, 2012                                                                       }
{ Website....                                                                                      }
{                                                                                                  }
{ Description A class that uses a Binary Space Partitioning tree for splitting up a matrix (e.g.   }
{             a game map) into smaller matrices. This is useful for making a map of dungeons where }
{             we can place rooms within the dimensions of the partitions for a randomized look.    }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
unit DungeonUnit;

{$mode objfpc}
{$M+}

interface
{--------------------------------------------------------------------------------------------------}
uses
	Math, RandomUnit;
{--------------------------------------------------------------------------------------------------}
type
	TBSPTree = class
	public
		X, Y, Width, Height: Word;
		LeftChild, RightChild: TBSPTree;
		Leaf: Boolean;
		constructor Create(FX, FY, FWidth, FHeight: Word);
		function Split: Boolean;
		procedure GenerateDungeon;
	end;
{--------------------------------------------------------------------------------------------------}
implementation
{--------------------------------------------------------------------------------------------------}
constructor TBSPTree.Create(FX, FY, FWidth, FHeight: Word);
begin
	X := FX;
	Y := FY;
	Width := FWidth;
	Height := FHeight;
	LeftChild := nil;
	RightChild := nil;
	Leaf := false;
end;
{--------------------------------------------------------------------------------------------------}
function TBSPTree.Split: Boolean;
const
	MinInterval = 0.35;
	MaxInterval = 0.65;
var
	Horizontal: Boolean;
	Cut: Word;
begin
	Randomize;
	Split := false;
	if LeftChild = nil then begin
		Horizontal := Random(2) = 1;
		if Horizontal then begin
			Cut := RandomRange(Round(Height * MinInterval), Round(Height * MaxInterval));
			LeftChild := TBSPTree.Create(X, Y, Width, Cut);
			RightChild := TBSPTree.Create(X, Y + Cut, Width, Height - Cut);
		end else begin
			Cut := RandomRange(Round(Width * MinInterval), Round(Width * MaxInterval));
			LeftChild := TBSPTree.Create(X, Y, Cut, Height);
			RightChild := TBSPTree.Create(X + Cut, Y, Width - Cut, Height);
		end;
		Split := true;
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure TBSPTree.GenerateDungeon;
const
	MinSize = 5;
var
	DX, DY, DWidth, DHeight: Word;
begin
	if LeftChild <> nil then begin
		LeftChild.GenerateDungeon;
		RightChild.GenerateDungeon;
	end else begin
// 		Randomize;
{
		if Width - MinSize <= 0 then DX := 0 else DX := Random(Width - MinSize);

		if Height - MinSize <= 0 then DY := 0 else DY := Random(Height - MinSize);

		DWidth := Max(Random(Width - DX), MinSize);

		DHeight := Max(Random(Height - DY), MinSize);}
		Leaf := true;
// 		Rectangle := TRectangle.Create(X, Y, Width, Height);

	end;
end;
{--------------------------------------------------------------------------------------------------}
initialization

finalization

end.