#pragma once

#include "entity.hpp"
#include <memory>
#include <vector>

namespace rogue {
	class Manager {
		private:
			std::vector<std::unique_ptr<Entity>> entities;

		public:
			Manager() {}
			~Manager() {}

			void addEntity(Entity*);

	};
}
