#pragma once

#include "component.hpp"

#include <array>
#include <bitset>
#include <memory>
#include <vector>

inline std::size_t getUniqueComponentID() {
	static std::size_t id = 0u;
	return id++;
}

template<typename Type>
inline std::size_t getComponentID() noexcept {
	//static_assert(std::is_base_of<Component, Type>::value, "Not a Component");
	static const std::size_t id = getUniqueComponentID();
	return id;
}

namespace rogue {
	class Entity {
		private:
			std::array<Component*, 32> componentArray;
			std::bitset<32> componentBitset;
			std::vector<std::unique_ptr<Component>> components;

		public:
			Entity() {}
			~Entity() {}

			template<typename Type, typename... Argument>
			inline Type& addComponent(Argument&&... arguments) {
				Type* component(new Type(std::forward<Argument>(arguments)...));

				std::unique_ptr<Component> pointer{component};
				components.emplace_back(std::move(pointer));

				componentArray[getComponentID<Type>()] = component;
				componentBitset[getComponentID<Type>()] = true;
				return *component;
			}

			template<typename Type>
			inline Type& getComponent() const {
				auto pointer(componentArray[getComponentID<Type>()]);
				return *static_cast<Type*>(pointer);
			}

			template<typename Type>
			inline bool hasComponent() const {
				return componentBitset[getComponentID<Type>()];
			}
	};
}
