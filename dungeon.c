
#define DIV_MIN 0.45
#define DIV_MAX 0.55

#define MIN_SIZE 4

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "bibliotek/tree.h"
#include "bibliotek/list.h"

typedef struct Part {
	int x1, y1, x2, y2;
} Part;

Part* part_init(int x1, int y1, int x2, int y2) {
	Part *part = (Part*)malloc(sizeof(Part));
	part->x1 = x1;
	part->y1 = y1;
	part->x2 = x2;
	part->y2 = y2;
	return part;
}

int rand_range(int low, int high) {
	if(low > high) {
		int tmp = low;
		low = high;
		high = tmp;
	}
	return rand() % (high - low + 1) + low;
}

void split_map(Tree *tree, int x1, int y1, int x2, int y2, int level) {

	int cut;

	if(level > 0) {

		// split horizontally or vertically
		if(rand() % 2) {

			//horizontal
			cut = y1 + rand_range((int)((y2 - y1) * DIV_MIN), (int)((y2 - y1) * DIV_MAX));

			tree_push_left(tree, (Item*)part_init(x1, y1, x2, cut - 1));
			tree_push_right(tree, (Item*)part_init(x1, cut, x2, y2));

			split_map(tree->left, x1, y1, x2, cut - 1, level - 1);
			split_map(tree->right, x1, cut, x2, y2, level - 1);

		}
		else {

			//vertical
			cut = x1 + rand_range((int)((x2 - x1) * DIV_MIN), (int)((x2 - x1) * DIV_MAX));
	
			tree_push_left(tree, (Item*)part_init(x1, y1, cut - 1, y2));
			tree_push_right(tree, (Item*)part_init(cut, y1, x2, y2));

			split_map(tree->left, x1, y1, cut -1, y2, level - 1);
			split_map(tree->right, cut, y1, x2, y2, level - 1);
		}
	}
}

void add_rooms(char** map, List *list) {

	Part *part;
	int a, b, x, y, w, h;

	while(part = list_pop_head(list)) {

		w = rand_range(MIN_SIZE, part->x2 - part->x1);
		a = rand_range(part->x1 + 1, part->x2 - w - 1);

		h = rand_range(MIN_SIZE, part->y2 - part->y1);
		b = rand_range(part->y1 + 1, part->y2 - h - 1);

// 		for(x = part->x1 + 1; x <= part->x2 - 1; x++) {
// 			for(y = part->y1 + 1; y <= part->y2 - 1; y++) {
// 				map[x][y] = '*';
// 			}
// 		}

		for(x = a; x <= w + a; x++) {
			for(y = b; y <= h + b; y++) {
				map[x][y] = '.';
			}
		}
	}
}
// printf("TEST4\n"); fflush(0);
void add_paths(char** map, Tree* tree, int level) {
	
}

void create_dungeon(char** map, int w, int h, int level) {

	// make a tree to store the splitted parts
	Tree *tree = tree_init((Item*)part_init(0, 0, w - 1, h - 1));

	// make a linked list to store the array of leaves
	List *list = list_init();

	// seed the randomiser function
	srand(time(NULL));
	
	// split the map into 2^level parts and store them in the tree
	split_map(tree, 0, 0, w - 1, h - 1, level);

	// create an array of the leaves 
	tree_get_leaves(tree, list);

	// make rooms in each of the splitted parts
	add_rooms(map, list);

	// walk in reverse order in the tree and create pathways beween
	// all the rooms
// 	add_paths(map, tree, level);

	list_free(list);
	tree_free(tree);	
}

int main(void) {
	
	char** map;
	int x, y;

	int mw = 80;
	int mh = 25;
	int l = 4;

	// make space in 2-dim array [100][100]
	map = malloc(sizeof(char*) * mw);
	for(x = 0; x < mw; x++) {
		map[x] = malloc(sizeof(char) * mh);
	}

	// fill map with '#' characters - this will represent walls
	for(x = 0; x < mw; x++) {
		for(y = 0; y < mh; y++) {
			map[x][y] = '#';
		}
	}

	// create rooms and corridors in the map
	create_dungeon(map, mw, mh, l);

// 	print the map so we can see what happended
	for(y = 0; y < mh; y++) {
		for(x = 0; x < mw; x++) {
			printf("%c", map[x][y]);
		}
		printf("\n");
	}

	for(x = 0; x < mw; x++) {
		free(map[x]);
	}
	free(map);
	
	return EXIT_SUCCESS;
}